*************
Custom Colors
*************

|Fill and Stroke dialog icon| :kbd:`Shift` + :kbd:`Ctrl` + :kbd:`F` :menuselection:`Object --> Fill and Stroke`

When the palette does not contain the color you would like to use, you can
select a color in the :guilabel:`Fill and Stroke` dialog. We will now take a
closer look at that dialog.

At the top of the dialog, there are 3 different tabs: for the :guilabel:`Fill`,
the :guilabel:`Stroke paint`, and the :guilabel:`Stroke style`.

In both the :guilabel:`Fill` and the :guilabel:`Stroke paint` tabs, you will find
6 different color selection modes: :guilabel:`RGB` (Red, Green, Blue),
:guilabel:`HSL` (Hue, Saturation, Lightness), :guilabel:`HSV` (Hue, Saturation, Value),
:guilabel:`CMYK` (Cyan, Magenta, Yellow, Key/Black), :guilabel:`Wheel` (Color wheel) and
:guilabel:`CMS` (Color Management System).

This book will not go into the details of the differences between these modes, but there are different use cases for them. Many people find the :guilabel:`HSL` color selector the easiest to use. The :guilabel:`CMS` color selector is reserved for advanced uses, and only makes sense in combination with the open source desktop publishing software `Scribus <https://www.scribus.net/>`_ .

For choosing a color for your first drawings, you can select any of the first
five options.

.. container:: two-column

   .. figure:: images/fill_and_stroke_RGB.png
       :alt: A light blue color in RGB color space
       :class: screenshot

       A light blue color in RGB color space, which is mostly used in monitors.

   .. figure:: images/fill_and_stroke_HSL.png
       :alt: A light blue color in HSL color space
       :class: screenshot

       The HSL color space is modelled after the way humans perceive colors.

   .. figure:: images/fill_and_stroke_HSV.png
       :alt: A light blue color in HSV color space
       :class: screenshot

       As with HSL, the HSV model is used for human perception of colors. It shows how colors are perceived under light.

   .. figure:: images/fill_and_stroke_CMYK.png
       :alt: A light blue color in CMYK color space
       :class: screenshot

       The same light blue color in CMYK color space, which is commonly used in printers.

   .. figure:: images/fill_and_stroke_Wheel.png
       :alt: A color wheel showing a light blue color in HSV color space
       :class: screenshot

       The color wheel offers an alternative way to select HSV values.

.. figure:: images/fill_and_stroke_dialog-fill_tab.png
   :alt: Blue mountains
   :class: screenshot

   Our mountains are light blue at the beginning.

To select your color, click or click-and-drag with the mouse in the different
sliders (or the wheel). Always remember to first select your object! The result
will immediately be displayed on the canvas. When you're happy with it, don't
touch anything in the dialog!

.. figure:: images/colors_change_fill.png
   :alt: Green mountains
   :class: screenshot

   But we prefer them green.

.. figure:: images/colors_change_stroke.png
   :alt: Green mountains with green stroke
   :class: screenshot

   And we choose a green stroke.

Every color chooser also has a field labelled :guilabel:`A` at the bottom. 'A' stands for 'Alpha', which is the opacity value of the selected color. The higher the value for alpha, the more opaque your color will be.

.. figure:: images/colors_change_alpha.png
   :alt: Green mountains with partially transparent fill
   :class: screenshot

   If we lower the alpha value for the fill, we can see that the sun is about to rise behind our green mountains. Note that the stroke is still completely opaque.

In case you were wondering about that little field labelled :guilabel:`RGBA` in the bottom right corner of the dialog: it is the name of the color in :term:`hexadecimal code <Hex Color>`, as it is used in a web page's :term:`CSS <Cascading Style Sheets>`. The last two letters appended to the code determine the :term:`opacity <Alpha>` or 'alpha value' (just like the slider labeled :guilabel:`A`).


.. |Fill and Stroke dialog icon| image:: images/icons/dialog-fill-and-stroke.*
   :class: header-icon
