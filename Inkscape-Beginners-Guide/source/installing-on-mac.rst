****************************
Installing Inkscape on a Mac
****************************


Installing Inkscape from the Inkscape website
=============================================

This is the recommended method for macOS users to install Inkscape.


#. Using a web browser, go to the
   `Inkscape downloads page <https://inkscape.org/release/>`_.

   .. image:: images/install_inkscape_download.png
      :class: screenshot

#. Click the box labelled :guilabel:`macOS` to find a list of downloads for macOS.

#. Depending upon the processor architecture of your Apple computer, choose the appropriate 
   Inkscape disk image (:term:`DMG <Disk Image (.dmg)>`) file for your system now.
   For the older, Intel-based, Apple computers, choose :guilabel:`dmg (Intel)`, 
   and for the newer, Arm64-based Apple computers, choose :guilabel:`dmg (arm64)`.

   .. image:: images/install_inkscape_download_mac.png
      :class: screenshot

#. Once the download has finished, open your :file:`Downloads` folder in the
   Finder.
   You can open this folder by selecting
   :menuselection:`Go --> Downloads` from the
   menu bar in the Finder. Double-click the Inkscape DMG file to open it.

   .. image:: images/install_mac_downloads.png
      :class: screenshot

#. Click and drag the Inkscape icon to the Applications icon as
   instructed in this window. This will install Inkscape on your Mac.

   .. image:: images/install_mac_dmg.png
      :class: screenshot

#. Open Inkscape by double-clicking its icon in the :guilabel:`Applications`
   folder.
   You can open the :guilabel:`Applications` folder by selecting
   :menuselection:`Go --> Applications` from the menu bar in the Finder.

   .. image:: images/install_mac_applications.png
      :class: screenshot


.. _inkscape092mac:

Installing Inkscape on OS X 10.7 - 10.10
========================================

If you have an older Mac running OS X version 10.7 - 10.10, you can still
install an older version of Inkscape, v 0.92. As part of this process, you will
first install a helper program called :term:`XQuartz` and then Inkscape 0.92
itself.


Step 1: Install XQuartz
-----------------------

#. Using a web browser, go to the `XQuartz website <https://www.xquartz.org/>`_
   and click the XQuartz DMG icon to download it.

   .. image:: images/install_xquartz_gotosite.png
      :class: screenshot

#. Once the download has finished, open your :file:`Downloads` folder in the
   Finder.
   You can open this folder by selecting :menuselection:`Go --> Downloads` from the menu bar in the Finder. Double-click the XQuartz.dmg file to open it.

   .. image:: images/install_xquartz_open_xq_dmg.png
      :class: screenshot

#. A new window will appear. Double-click the XQuartz.pkg icon to launch the
   XQuartz installer. Follow the steps and instructions in installer to finish installing XQuartz.

   .. image:: images/install_xquartz_pkg.png
      :class: screenshot


#. Restart, shut down, or log out of your Mac before proceeding to the next step.

   .. image:: images/install_xquartz_shut_down.png
      :class: screenshot


Step 2: Install Inkscape
------------------------

#. Using a web browser, go to the
   `Inkscape 0.92.2 downloads page <https://inkscape.org/release/0.92.2/mac-os-x/>`_.

   .. image:: images/install_inkscape_092_download.png
      :class: screenshot

#. Click the box labelled :guilabel:`Mac OS X 10.7 Installer (xquartz)` to
   download the DMG file, which contains the installer.

#. Once the download has finished, open your :file:`Downloads` folder in the Finder.
   Once again, you can open this folder by selecting
   :menuselection:`Go --> Downloads` from the
   menu bar in the Finder. Double-click the Inkscape DMG file to open it.

   .. image:: images/install_inkscape_open_is_dmg.png
      :class: screenshot

#. Click and drag the Inkscape icon to the Applications icon as
   instructed in this window. This will install Inkscape on your Mac.

   .. image:: images/install_inkscape_092_mac_drag.png
      :class: screenshot


Step 3: Setting Up Inkscape
---------------------------

#. Open Inkscape by double-clicking its icon in the :guilabel:`Applications`
   folder.
   You can open the :guilabel:`Applications` folder by selecting
   :menuselection:`Go --> Applications` from the menu bar in the Finder.

   .. image:: images/set_up_inkscape_open_is.png
      :class: screenshot

#. Click OK in this window, which appears the first time Inkscape is
   opened. Wait for Inkscape to open. This might take a few minutes, since
   Inkscape is scanning all the font files in your system. The next time you
   open Inkscape, it will not take nearly as long to show up.

   .. image:: images/set_up_inkscape_fyi_click_ok.png
      :class: screenshot

#. Once Inkscape does open, its interface will appear inside XQuartz. When
   Inkscape is running, you will see the name "Inkscape" in the title bar of
   your window, but the menu bar will show the name "XQuartz" .

   .. image:: images/set_up_inkscape_is_interface.png
      :class: screenshot

#. With Inkscape open, select the XQuartz preferences by selecting
   :menuselection:`XQuartz --> Preferences...` from the menu bar.

   .. image:: images/set_up_inkscape_open_is_preferences.png
      :class: screenshot

#. Click the Input tab, and configure its settings as shown here.

   .. image:: images/set_up_inkscape_input_prefs.png
      :class: screenshot

#. Click the Pasteboard tab, and configure its settings as shown here.

   .. image:: images/set_up_inkscape_pasteboard_prefs.png
      :class: screenshot

#. Close Preferences. You are now ready to use Inkscape.

Note that when using Inkscape version 0.92 with XQuartz, its keyboard shortcuts
use the **control** key, rather than the usual command key.


Homebrew and MacPorts
=====================

In addition to the preceding "standard" methods of installing Inkscape, you can
also build Inkscape on your Mac using Homebrew or MacPorts. These methods are
intended for users who prefer these environments; most users should use the
recommended methods above. You will need to be familiar with the macOS terminal
and you may need to `install Xcode
<https://guide.macports.org/#installing.xcode>`_ and its command line developer
tools in order to use these methods. Depending on the type of Inkscape build,
you may also need XQuartz.


Using Homebrew
--------------

#. Install Homebrew, using the instructions at `the main Homebrew site <https://brew.sh>`_.

#. See the `Homebrew instructions for Inkscape 
   <https://formulae.brew.sh/cask/inkscape>`_ for the specific
   Homebrew formula to use.


Using MacPorts
--------------

Install MacPorts. Read `the guide <https://guide.macports.org/#installing.macports>`_
on how to do so, since there are different methods and requirements
depending on your operating system version.

