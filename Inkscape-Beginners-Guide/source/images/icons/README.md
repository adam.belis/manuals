*****
Icons
*****

The icons in this directory are modified (minimized, converted) 
original icons from the Inkscape program.

As such, they are licensed under the GNU Public License Version 2 or later.
