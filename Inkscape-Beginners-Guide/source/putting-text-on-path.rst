**********************
Putting Text on a Path
**********************

The adjustment of letters' and words' positions are merely the finishing
touch. More often, it is useful to start out by layouting the text as a
whole in a drawing. Inkscape offers two options for this, which are
accessible via :menuselection:`Text --> Put on Path` and :menuselection:`Text --> Flow into Frame`.

|image0|

To make a text that follows a path's curvature, you need to do this:

#. Create a path that will serve as the base line for the words and
   letters of the text. The path can be created by using the shape
   tools and combining shapes with the Boolean operations, or by
   drawing it directly with one of the tools that draw paths.
#. Write the text directly on the canvas, after clicking on it with
   the mouse. It's not useful to use flowed text in a frame for this.
#. With the Selector tool, select the path and the text. Then make
   Inkscape do the work by selecting :menuselection:`Text --> Put on
   Path` from the menu.

The result will show up on the canvas immediately. The text can still be
edited at any time: you can change its contents or style whenever you
like. The path will remain editable, too: you can shape its curves, add
or delete nodes, move it, etc. and the text will automatically adjust
to its changed shape.

Very often, this path only serves to position the text. If you delete
it, the text doesn't have anything to follow anymore, and reverts to its
previous shape. If you don't want to see the path in your drawing,
remove its fill and its stroke!

.. figure:: images/put_on_path_A.png
    :alt: Creating a text and a path
    :class: screenshot

    Create a text and a path by whichever method you prefer…

.. figure:: images/put_on_path_B.png
    :alt: Putting the text on the path
    :class: screenshot

    … and put the text on the path using :menuselection:`Text --> Put on Path`.

.. |image0| image:: images/icons/text-put-on-path.*
   :class: header-icon
