********************
Align and Distribute
********************

|Large Icon for Align and Distribute dialog| :kbd:`Shift` + :kbd:`Ctrl` + :kbd:`A`

This dialog offers invaluable help. You can use it to align nodes and
objects vertically or horizontally, or to distribute them at equal
distances.

To use it for aligning nodes:

#. Open the dialog by clicking on its icon in the command bar to the right of the canvas.
#. Switch to the Node tool.
#. Select a couple of nodes.
#. Click on the node alignment button of your choice. The alignment will be applied immediately.

To align objects:

#. Open the dialog.
#. Switch to the Select tool.
#. Select the objects that you want to align.
#. Decide which one of the objects should not move at all. The object you
   selected first? The one you selected last? The biggest or the smallest
   one? Or perhaps you want to move all the objects in relation to the page? In
   the dropdown labelled :guilabel:`Relative to:`, select the option that
   applies.
#. Click on the alignment button of your choice. Use the tooltips that display
   when you hover over them with the mouse to learn what each symbol means. The
   selected alignment will be applied immediately.

.. figure:: images/chaos.png
   :alt: Disordered objects
   :class: screenshot

   Before arranging the objects, they are distributed somewhat randomly on the
   canvas.

.. figure:: images/order.png
   :alt: Ordered objects
   :class: screenshot

   After aligning each row and column vertically and horizontally, the
   result looks very neat.

.. figure:: images/align_distribute_dialog.png
   :alt: The Align and Distribute dialog in object mode
   :class: screenshot

   With the :guilabel:`Align and Distribute dialog`, it only takes a couple of clicks to arrange your objects neatly.



.. |Large Icon for Align and Distribute dialog| image:: images/icons/dialog-align-and-distribute.*
   :class: header-icon
