# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2018-2023, Inkscape Documentation Authors
# This file is distributed under the same license as the Inkscape Beginners'
# Guide package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2023.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Inkscape Beginners' Guide 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-11 16:48+0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language: zh_TW\n"
"Language-Team: zh_TW <LL@li.org>\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.1\n"

#: ../../source/spirals.rst:3
msgid "Spirals"
msgstr ""

#: ../../source/spirals.rst:5
msgid "|Icon for Spiral Tool| :kbd:`F9` or :kbd:`I`"
msgstr ""

#: ../../source/spirals.rst:75
msgid "Icon for Spiral Tool"
msgstr ""

#: ../../source/spirals.rst:7
msgid ""
"This geometrical shape isn't needed frequently, but sometimes, it proves "
"to be very useful."
msgstr ""

#: ../../source/spirals.rst:10
msgid ""
"To draw a spiral, click and drag with the mouse on the canvas. When the "
"left mouse button is released, the spiral will be finished. You will "
"notice two diamond-shaped handles on the spiral."
msgstr ""

#: ../../source/spirals.rst:12
msgid ""
"These handles change the length of the spiral. You can try this out right"
" on the canvas: Just grab one of the handles and drag it along the "
"spiral's turns to get the desired number of turns and to make the spiral "
"larger or smaller."
msgstr ""

#: ../../source/spirals.rst:17
msgid ""
"If you hold down :kbd:`Ctrl` while dragging the handles, the spiral will "
"get longer (or shorter) in 15° steps."
msgstr ""

#: ../../source/spirals.rst:21
msgid ""
"When you combine pressing :kbd:`Alt` with pulling the inner handle "
"upwards or downwards, it will change the divergence (tightness) / "
"convergence (looseness) of the spiral, without changing its overall size."
" Dragging upwards makes the turns move toward the outside of the spiral. "
"Dragging downwards will make them move closer to its center."
msgstr ""

#: ../../source/spirals.rst:27
msgid ""
"The easiest way to change the number of turns of a spiral quickly by a "
"large amount is to enter the desired number into the field labelled "
":guilabel:`Turns` in the tool controls bar. This will not change the "
"spiral's diameter."
msgstr ""

#: ../../source/spirals.rst:32
msgid ""
"If you ever feel lost when working with the spiral tool, you can use the "
"|Reset Icon| rightmost icon in the tool controls bar to remove all "
"changes and to reset the spiral to its initial shape."
msgstr ""

#: ../../source/spirals.rst:77
msgid "Reset Icon"
msgstr ""

#: ../../source/spirals.rst
msgid "Spiral tool"
msgstr ""

#: ../../source/spirals.rst:42
msgid "A basic spiral"
msgstr ""

#: ../../source/spirals.rst
msgid "Spiral tool: changing with the handles"
msgstr ""

#: ../../source/spirals.rst:48
msgid "Changing a spiral using its handles."
msgstr ""

#: ../../source/spirals.rst
msgid "Spiral tool: drag inner handle down to converge"
msgstr ""

#: ../../source/spirals.rst:54
msgid ""
":kbd:`Alt` + dragging the inner handle downwards makes the spiral "
"converge more."
msgstr ""

#: ../../source/spirals.rst
msgid "Spiral tool: drag inner handle up to diverge"
msgstr ""

#: ../../source/spirals.rst:60
msgid ""
":kbd:`Alt` + dragging the inner handle upwards lets the spiral more "
"divergent."
msgstr ""

#: ../../source/spirals.rst
msgid "Spiral tool: large number of turns"
msgstr ""

#: ../../source/spirals.rst:66
msgid "A spiral with a large number of turns."
msgstr ""

#: ../../source/spirals.rst
msgid "Spiral tool: small number of turns"
msgstr ""

#: ../../source/spirals.rst:72
msgid "A spiral with a smaller number of turns."
msgstr ""

