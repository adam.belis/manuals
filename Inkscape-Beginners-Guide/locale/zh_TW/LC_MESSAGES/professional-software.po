# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2018-2023, Inkscape Documentation Authors
# This file is distributed under the same license as the Inkscape Beginners'
# Guide package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2023.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Inkscape Beginners' Guide 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-11 16:48+0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language: zh_TW\n"
"Language-Team: zh_TW <LL@li.org>\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.1\n"

#: ../../source/professional-software.rst:3
msgid "Professional Software"
msgstr ""

#: ../../source/professional-software.rst:5
msgid ""
"Inkscape is powerful but doesn't require a recent, high-end computer to "
"be usable. It has many drawing tools, filters, effects and patterns."
msgstr ""

#: ../../source/professional-software.rst:7
msgid ""
"Many professionals use Inkscape for vector image editing. It fits easily "
"into a design workflow with open source or proprietary tools."
msgstr ""

#: ../../source/professional-software.rst:10
msgid "What am I allowed to do with it?"
msgstr ""

#: ../../source/professional-software.rst:12
msgid ""
"Inkscape is **free and open source software** (`Wikipedia "
"<https://en.wikipedia.org/wiki/Free_and_open-source_software>`_). It can "
"be `downloaded <https://inkscape.org/releases/>`_ and installed on "
":doc:`Windows <installing-on-windows>`, :doc:`Mac <installing-on-mac>`, "
"and :doc:`Linux <installing-on-linux>`. Copying, distribution, and "
"modification of the software are freely permitted. Inkscape can be used "
"at work or at home, for professional or personal work. What you create "
"with it is entirely yours, and you are free to distribute, share it, sell"
" it, however you’d like — or keep it to yourself! Except if you used "
"someone else’s work as a basis for yours, in which case this person is "
"also a co-author of the work."
msgstr ""

#: ../../source/professional-software.rst:22
msgid "How much does it cost?"
msgstr ""

#: ../../source/professional-software.rst:24
msgid ""
"Inkscape is **gratis**, that is, cost-free: you can download and install "
"it as many times as you want without paying either a fixed fee or a "
"subscription, and give it away to friends, provided you agree with the "
"free software GNU General Public License (GPL). The only investment "
"you'll need to make is to **learn** to use it and gain **new skills**."
msgstr ""

#: ../../source/professional-software.rst:31
msgid ""
"You can also `donate <https://inkscape.org/support-us/>`_ to allow the "
"project volunteers to dedicate more time to it."
msgstr ""

